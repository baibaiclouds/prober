
var mainObj = {
  authkeydownsec: 0,
  authkeyinfo: null,
  authackInfo: null,
  lastAuthkeyStartTimeoutId: 0,
  init: function () {
    M.AutoInit();
    $("#tool-close-btn").click(mainObj.hideapp);
  },
  hideapp: function () {
    window.appruntime.hideapp();
  }
}

$(function () {
  mainObj.init();
});