# 百百|WEB 堡垒机-客户端(英文名称：baibai)

整个基于 web 的远程控制，无任何插件。协议支持 vnc，rdp，ssh。支持内外穿透、四层协议代理。

当前项目是客户端，服务端[代码](https://gitee.com/baibaiclouds/platform)https://gitee.com/baibaiclouds/platform

[官网地址 http://bb.app-yun.com/](http://bb.app-yun.com/)

# 客户端界面

![1](https://img-blog.csdnimg.cn/07ddd1db71004797ac0603e44d8a0b59.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5rOV5Y-377ya5ZCD6IKJ,size_20,color_FFFFFF,t_70,g_se,x_16)

# 源码启动

yarn

yarn dev

# 插个私人广告 BB-API HTTP 请求工具

致力于打造简洁、免费、好用的 HTTP 模拟请求工具，自动生成接口文档。

帮助您公司、团队、个人提高开发效率。

官网地址：http://api.app-yun.com/bbapi/index.html
